source 'https://rubygems.org'

ruby "2.2.3"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
# Use postgresql as the database for Active Record
gem 'pg'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'

gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc

#deploy
gem 'chef'
gem 'chef-zero'
gem 'knife-solo'
gem 'librarian-chef'

#api docs
gem "swagger-blocks"
gem "swagger_engine"
gem "active_model_serializers"

gem "sidekiq"
gem "sinatra", require: nil
gem "scaledrone"

gem "redis-rails"

gem 'rake'
gem 'simple_form'
gem 'nokogiri'
gem 'seedbank'
gem 'sorcery'
gem 'faraday'
gem 'figaro'
gem 'carrierwave'
gem 'mini_magick'
gem 'fog-aws'
gem 'rails_12factor', group: :production
gem 'levenshtein'
gem 'whenever', require: false
gem 'http_accept_language'
gem 'routing-filter'
gem 'rollbar'
gem 'newrelic_rpm'
gem "puma"
# gem "thin"
# gem "codeclimate-test-reporter", group: :test, require: nil
gem "responders"

#admin panel
gem "activeadmin", github: "activeadmin"
gem "pundit"
gem "rolify"

#flickr support
gem "flickraw-cached"
gem 'slick_rails'
gem "i18n-js", ">= 3.0.0.rc11"

group :test do
  gem "rspec-sidekiq"
  gem "json_matchers"
  gem "webmock"
end

group :development, :test do
  gem 'rspec-rails'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem "database_cleaner"
  gem "poltergeist"
end
