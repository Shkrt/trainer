/*jshint multistr: true */
var content = '<div class="form-group"><div class="controls row-fluid"><div class="form-inline"><input type="text" class="form-control" placeholder="Enter your keyword" id="search_field"><button class="btn btn-default" id="flickr_show_btn" data-method="POST" data-remote="true">Search images</button></div></div></div>';
var url = "/image_search";
var idle = "<p class='text-center'><h1>LOADING...</h1></p>";
var selectBtn = '<div class="btn-container"><div class="text-center"><a id="select-image" class="btn btn-default">Select</a></div></div>';

$(document).ready(function() {
  $(".thumbnail > img").bind("error", function(e) {
    $(".thumbnail").text(I18n.t("js.missing_image"));
  });
  $("#flickr_search_btn").unbind().on("click", function() {
    if ($("#search_field").size() === 0) {
      $("#images").after(content);
      $("#flickr_show_btn").unbind().on("click", function(e) {
        e.preventDefault();
        var value = $("#search_field").val();
        if (value !== "") {
          clearImageSet(".multiple-items");
          $.ajax({
            type: "POST",
            url: url,
            data: "&keyword=" + value,
            success: function(msg, status, jqXHR) {
              inflateView(msg, ".multiple-items");
            }
          });
        }
      });
    }
  });
});

function inflateView(response, element) {
  $(element).empty();
  var data = Object.keys(response).map(function(key) {
    return response[key];
  })[0];
  var len = data.length;
  if (len > 0) {
    for (var i = 0; i < len; i++) {
      $(element).append("<div><img class='picture' src='" + data[i] + "'></div>");
    }
    makeSlick($(element));
    $(".picture").on("click", function() {
      var link = $(this).attr("src");
      $(".slick-slide > .btn-container").remove();
      $(this).after(selectBtn);
      $("#select-image").on("click", function() {
        var cardJson = {
          card: {
            remote_image: link
          }
        };
        $.ajax({
          type: "PATCH",
          url: "/cards/" + card_id,
          data: cardJson,
          dataType: "script"
        });
      });
    });
  } else {
    $(element).removeClass("jumbotron");
  }
}

function clearImageSet(tag) {
  if ($(tag).children().length > 0) {
    $(tag).slick("unslick");
    $(tag).empty();
  }
  $(tag).addClass("jumbotron");
  $(tag).append(idle);
}

function makeSlick(element) {
  $(element).slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    centerMode: true,
    variableWidth: true
  });
}
