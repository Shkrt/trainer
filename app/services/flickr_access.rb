class FlickrAccess
  def initialize
    FlickRaw.api_key = ENV["FLICKR_KEY"]
    FlickRaw.shared_secret = ENV["FLICKR_SECRET"]
    @flickr = FlickRaw::Flickr.new
  end

  def get_by_keyword(keyword, amount)
    (@flickr.photos.search text: keyword).take(amount).map { |x| FlickRaw.url_m(x) }
  end
end
