class CardsFetch
  def self.grab(url, original, translated)
    doc = Nokogiri::HTML(open(url))
    original_words = doc.css(original)
    translated_words = doc.css(translated)
    original_words.zip(translated_words)
  rescue Nokogiri::CSS::SyntaxError
    nil
  end
end

# http://1000mostcommonwords.com/1000-most-common-welsh-words/
# td:nth-child(3)
# td:nth-child(2)
