module FooterHelper
  def footer(logged_in)
    content_tag(:div, class: "footer") do
      content_tag(:div, class: "container") do
        concat(locale_switch(logged_in))
        concat(api_docs)
      end
    end
  end

  def locale_switch(user)
    unless user
      content_tag(:p, class: "navbar-text pull-right") do
        concat(link_to "en", root_path(user_locale: "en"))
        concat " | "
        concat(link_to "ru", root_path(user_locale: "ru"))
      end
    end
  end

  def api_docs
    content_tag(:p, class: "navbar-text pull-left") do
      link_to t("api_v1_link"), swagger_engine_path
    end
  end
end
