module TrainerHelper
  def trainer_image(card)
    img = card.image_url || card.remote_image
    card_image(img) if img
  end
end
