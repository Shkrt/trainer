module CardHelper
  def card_image(url)
    content_tag(:div, class: "row form-group") do
      content_tag(:div, class: "col-xs-6 col-md-4 card_image") do
        content_tag(:p, class: "thumbnail") do
          image_tag url
        end
      end
    end
  end

  def flickr_btn
    content_tag(:div, class: "form-group", id: "images") do
      content_tag(:button, class: "btn btn-default", id: "flickr_search_btn", type: "button") do
        t("card.search_image")
      end
    end
  end

  def remove_remote(card)
    content_tag(:div, class: "form-group") do
      content_tag(:button, class: "btn btn-default", type: "button") do
        link_to t("card.remove_image"), remove_picture_path(id: card.id), method: :post
      end
    end
  end
end
