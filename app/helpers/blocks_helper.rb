module BlocksHelper
  def block_status(block)
    if block.current?
      content_tag :td do
        link_to t("block.reset_current"), reset_as_current_block_path(block), method: :put
      end
    else
      content_tag :td do
        link_to t("block.mark_as_current"), set_as_current_block_path(block), method: :put
      end
    end
  end
end
