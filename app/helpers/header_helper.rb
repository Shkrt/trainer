module HeaderHelper
  def brand_link(logged_in)
    if logged_in
      link_to t(:flashcards_label), trainer_path, class: "navbar-brand"
    else
      link_to t(:flashcards_label), root_path, class: "navbar-brand"
    end
  end

  def left_navbar(logged_in)
    if logged_in
      content_tag(:ul, class: "nav navbar-nav navbar-left") do
        concat(content_tag(:li) { link_to t(:all_decks_label), blocks_path })
        concat(content_tag(:li) { link_to t(:add_deck_label), new_block_path })
        concat(content_tag(:li) { link_to t(:all_cards_label), cards_path })
        concat(content_tag(:li) { link_to t(:add_card_label), new_card_path })
        concat(content_tag(:li) { link_to t(:add_cards_from_remote), new_path })
      end
    end
  end

  def right_navbar(logged_in)
    content_tag(:p, class: "navbar-text navbar-right") do
      logged_in ? right_nav_logged(logged_in.has_role?(:admin)) : right_nav_not_logged
    end
  end

  private

  def right_nav_logged(admin)
    concat(link_to t(:user_profile_label), edit_profile_path(current_user), class: "navbar-link")

    if admin
      concat " | "
      concat(link_to t("active_admin.dashboard"), :admin_root, class: "navbar-link")
    end

    concat " | "
    concat(link_to t(:log_out_label), :logout, method: :post, class: "navbar-link")
  end

  def right_nav_not_logged
    concat(link_to t(:sing_up_label), new_user_path, class: "navbar-link")
    concat(" | ")
    concat(link_to t(:log_in_with_github_label), auth_at_provider_path(provider: :github), class: "navbar-link")
    concat(" | ")
    concat(link_to t(:log_in_label), :login, class: "navbar-link")
  end
end
