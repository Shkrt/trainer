class UserPolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def resolve
      record.all
    end
  end
end
