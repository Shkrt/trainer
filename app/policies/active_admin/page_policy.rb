module ActiveAdmin
  class PagePolicy < ApplicationPolicy
    class Scope < Struct.new(:user, :scope)
      def resolve
        scope
      end
    end

    def show?
      record.name == "Dashboard" ? user.has_role?(:admin) : false
    end
  end
end
