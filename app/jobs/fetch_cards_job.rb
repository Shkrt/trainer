class FetchCardsJob < ActiveJob::Base
  queue_as :default

  def perform(params, user)
    collection = CardsFetch.grab(params[:remote_url],
                                 params[:original_selector],
                                 params[:translated_selector])
    if collection
      collection.map do |x, y|
        Card.create(original_text: x.text.downcase,
                    translated_text: y.text.downcase,
                    user_id: user.id, block_id: user.blocks.first.id)
      end
    end
  end

  after_perform do |_j|
    sd = ScaleDrone.new(channel_id: ENV["SCALEDRONE_CHANNEL_ID"],
                        secret_key: ENV["SCALEDRONE_SECRET_KEY"])
    sd.publish("notifications", msg: "Cards fetching job was finished")
  end
end
