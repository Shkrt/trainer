class ApplicationController < ActionController::Base
  include Pundit
  respond_to :html

  protect_from_forgery with: :exception
  before_action :set_locale

  private

  def not_authorized(arg)
    redirect_to root_path, alert: t(:not_authorized_alert)
  end

  def authenticate!
    redirect_to login_path, alert: t(:please_log_in) unless current_user
  end

  def set_locale
    locale = if current_user
               current_user.locale
             else
               session[:locale] = params[:user_locale] || session[:locale] ||
               http_accept_language.compatible_language_from(I18n.available_locales)
             end

    if locale && I18n.available_locales.include?(locale.to_sym)
      session[:locale] = I18n.locale = locale.to_sym
    end
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end
end
