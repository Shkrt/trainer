module Api
  class ApiController < ActionController::Base
    attr_reader :current_user
    include ActionController::Serialization
    include ActionController::HttpAuthentication::Basic::ControllerMethods
    before_action :authenticate_basic

    private

    def authenticate_basic
      authenticate_or_request_with_http_basic do |email, _passwd|
        user = User.where(email: email).first
        if user
          @current_user = user
        else
          render json: { message: "Authentication failed" }, status: 401
        end
      end
    end
  end
end
