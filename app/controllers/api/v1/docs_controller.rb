module Api
  module V1
    class DocsController < ActionController::Base
      include Swagger::Blocks
      def index
        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
      end
    end
  end
end
