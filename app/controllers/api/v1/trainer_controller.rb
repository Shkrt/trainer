module Api
  module V1
    class TrainerController < Api::ApiController
      include Swagger::Blocks

      swagger_path "/review_card" do
        operation :put do
          key :description, "Checks one card's translation"
          key :produces, ["application/json"]
          key :tags, ["Trainer"]

          parameter do
            key :name, :id
            key :in, :body
            key :description, "Id of checked card"
            key :required, true
            key :type, :integer
            key :format, :int64
          end

          parameter do
            key :name, :user_translation
            key :in, :body
            key :description, "User translation"
            key :required, true
            key :type, :string
          end

          response 200 do
            key :description, "One card for review"
            schema do
              key :"$ref", :ReviewResult
            end
          end

          response 401 do
            key :description, "Not Authenticated"
            schema do
              key :"$ref", :NotAuthenticated
            end
          end
        end
      end

      swagger_path "/trainer" do
        operation :get do
          key :description, "Takes one pending card"
          key :produces, ["application/json"]
          key :tags, ["Trainer"]

          response 200 do
            key :description, "One card for review"
            schema do
              key :"$ref", :Card
            end
          end

          response 401 do
            key :description, "User Not Authenticated"
            schema do
              key :"$ref", :UserNotAuthenticated
            end
          end
        end
      end

      def index
        @card = params[:id] ? current_user.cards.find(params[:id]) : current_user.get_card
        render json: @card, serializer: ::CardSerializer
      end

      def review_card
        @card = current_user.cards.find(params[:card_id])
        result = @card.check_translation(trainer_params[:user_translation])

        render json: { conclusion: result[:message][:text], state: result }
      end

      private

      def trainer_params
        params.permit(:user_translation)
      end
    end
  end
end
