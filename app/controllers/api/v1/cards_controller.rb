module Api
  module V1
    class CardsController < Api::ApiController
      include Swagger::Blocks

      swagger_path "/cards" do
        operation :get do
          key :description, "Returns all of current user's cards"
          key :produces, ["application/json"]
          key :tags, ["Cards"]

          response 200 do
            key :description, "Collection of cards"
            schema do
              key :"$ref", :Cards
            end
          end

          response 401 do
            key :description, "Not Authenticated"
            schema do
              key :"$ref", :NotAuthenticated
            end
          end
        end

        operation :post do
          key :description, "Creates new card"
          key :produces, ["application/json"]
          key :tags, ["Cards"]

          parameter do
            key :name, :card
            key :in, :body
            key :description, "Card to add"
            key :required, true
            schema do
              key :"$ref", :CardInput
            end
          end

          response 200 do
            key :description, "New card"
            schema do
              key :"$ref", :Cards
            end
          end

          response 401 do
            key :description, "Not Authenticated"
            schema do
              key :"$ref", :NotAuthenticated
            end
          end

          response :default do
            key :description, "unexpected error"
            schema do
              key :"$ref", :ErrorModel
            end
          end
        end
      end

      def index
        @cards = current_user.cards.all.order("review_date")
        render json: @cards, each_serializer: ::CardSerializer
      end

      def create
        @card = current_user.cards.build(card_params)
        if @card.save
          render json: @card, serializer: ::CardSerializer
        else
          render nothing: true, status: 400
        end
      end

      private

      def card_params
        params.require(:card).permit(:original_text, :translated_text,
                                     :review_date, :image_cache, :block_id)
      end
    end
  end
end
