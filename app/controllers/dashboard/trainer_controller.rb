class Dashboard::TrainerController < Dashboard::BaseController
  
  def index
    @card = params[:id] ? current_user.cards.find(params[:id]) : current_user.get_card

    respond_to do |format|
      format.html
      format.js
    end
  end

  def review_card
    @card = current_user.cards.find(params[:card_id])
    result = @card.check_translation(trainer_params[:user_translation])
    flash[result[:message][:type]] = result[:message][:text]

    result[:state] ? (redirect_to trainer_path) : (redirect_to trainer_path(id: @card.id))
  end

  private

  def trainer_params
    params.permit(:user_translation)
  end

end
