class Dashboard::CardsController < Dashboard::BaseController
  before_action :set_card, only: [:destroy, :edit, :update]

  def index
    @cards = current_user.cards.all.order('review_date')
  end

  def new
    @card = Card.new
  end

  def edit
  end

  def create
    @card = current_user.cards.build(card_params)
    if @card.save
      redirect_to cards_path
    else
      respond_with @card
    end
  end

  def update
    if @card.update(card_params)
       respond_to do |format|
         @cards = current_user.cards
         format.html { redirect_to cards_path }
         format.js { render :index }
       end
    else
      respond_with @card
    end
  end

  def destroy
    @card.destroy
    respond_with @card
  end

  def remove_picture
    @card=Card.find(params[:id])
    @card.remote_image = nil
    @card.save
    redirect_to edit_card_path(@card)
  end

  private

  def set_card
    @card = current_user.cards.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:original_text, :translated_text, :review_date,
                  :image, :image_cache, :remove_image, :block_id, :remote_image)
  end
end
