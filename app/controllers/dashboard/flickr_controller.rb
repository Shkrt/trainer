class Dashboard::FlickrController < Dashboard::BaseController
  def search
    img_json = Rails.cache.fetch("#{params[:keyword]}", expires_in: 6.hours) do
      FlickrAccess.new.get_by_keyword(params[:keyword], 10)
    end

    render json: img_json
  end
end
