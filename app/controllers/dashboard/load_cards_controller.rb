module Dashboard
  class LoadCardsController < Dashboard::BaseController
    before_action :has_blocks?, only: [:new]

    def new
    end

    def fetch_cards
      FetchCardsJob.perform_later load_card_params, current_user
      redirect_to root_path, notice: t(:job_enqueued)
    end

    private

    def load_card_params
      params.permit(:remote_url, :original_selector, :translated_selector)
    end

    def has_blocks?
      redirect_to new_block_path, alert: t(:no_blocks_alert) unless current_user.blocks.any?
    end
  end
end
