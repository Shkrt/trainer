module Api
  module V1
    class TrainerModel < ActiveRecord::Base
      include Swagger::Blocks

      swagger_schema :ReviewResult do
        key :required, [:conclusion, :state]
        property :conclusion do
          key :type, :string
        end
        property :state do
          key :type, :boolean
        end
      end
    end
  end
end
