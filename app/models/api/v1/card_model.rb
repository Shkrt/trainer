module Api
  module V1
    class CardModel < ActiveRecord::Base
      include Swagger::Blocks

      swagger_schema :Cards do
        key :required, [:card]
        property :card do
          key :type, :Card
          key :format, :array
        end
      end

      swagger_schema :Card do
        key :required, [:original_text, :translated_text, :review_date, :id]
        property :original_text do
          key :type, :string
        end
        property :translated_text do
          key :type, :string
        end
        property :review_date do
          key :type, :string
        end
        property :id do
          key :type, :integer
          key :format, :int64
        end
      end

      swagger_schema :CardInput do
        key :required, [:original_text, :translated_text, :block_id]
        property :original_text do
          key :type, :string
        end
        property :translated_text do
          key :type, :string
        end
        property :block_id do
          key :type, :integer
          key :format, :int64
        end
      end
    end
  end
end
