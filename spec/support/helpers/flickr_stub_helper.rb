module FlickrStubHelper
  def raw_response
    { "photos": { "page": 1, "pages": 15, "perpage": 100, "total": "1490",
      "photo": [
      { "id": "11111111111", "owner": "133333322@N02", "secret": "aaaaaaaaa", "server": "257", "farm": 1, "title": "dr. Alan Grant", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "00000000000", "owner": "9100000@N00", "secret": "bbbbbbbbb", "server": "7411", "farm": 8, "title": "avn copy", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "22222222222", "owner": "91344444@N00", "secret": "ccccccccc", "server": "8650", "farm": 9, "title": "IMG_1035", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "33333333333", "owner": "91776868@N00", "secret": "ddddddddd", "server": "8569", "farm": 9, "title": "serious about this mock-up", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "44444444444", "owner": "943434@N00", "secret": "eeeeeeeee", "server": "5586", "farm": 6, "title": "Kane primer coat", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "55555555555", "owner": "9133338@N00", "secret": "fffffffff", "server": "3871", "farm": 4, "title": "Tooled leather bolero", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "66666666666", "owner": "17222222@N00", "secret": "ggggggggg", "server": "2905", "farm": 3, "title": "Katharine Isabelle", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "77777777777", "owner": "411111@N06", "secret": "hhhhhhhhh", "server": "2816", "farm": 3, "title": "AmMay-997", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "88888888888", "owner": "4934324@N06", "secret": "iiiiiiiii", "server": "7373", "farm": 8, "title": "Amy-1", "ispublic": 1, "isfriend": 0, "isfamily": 0 },
      { "id": "99999999999", "owner": "4345345@N06", "secret": "jjjjjjjjj", "server": "2835", "farm": 3, "title": "Amer-2", "ispublic": 1, "isfriend": 0, "isfamily": 0 }
    ] }, "stat": "ok" }.to_json
  end

  def parsed_response
    ["https://farm1.staticflickr.com/257/11111111111_aaaaaaaaa_m.jpg",
     "https://farm8.staticflickr.com/7411/00000000000_bbbbbbbbb_m.jpg",
     "https://farm9.staticflickr.com/8650/22222222222_ccccccccc_m.jpg",
     "https://farm9.staticflickr.com/8569/33333333333_ddddddddd_m.jpg",
     "https://farm6.staticflickr.com/5586/44444444444_eeeeeeeee_m.jpg",
     "https://farm4.staticflickr.com/3871/55555555555_fffffffff_m.jpg",
     "https://farm3.staticflickr.com/2905/66666666666_ggggggggg_m.jpg",
     "https://farm3.staticflickr.com/2816/77777777777_hhhhhhhhh_m.jpg",
     "https://farm8.staticflickr.com/7373/88888888888_iiiiiiiii_m.jpg",
     "https://farm3.staticflickr.com/2835/99999999999_jjjjjjjjj_m.jpg"]
  end

  def cached_response
    { "doesn't": "matter" }.to_json
  end
end
