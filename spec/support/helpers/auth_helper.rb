module AuthHelper
  def basic_auth(mail, pw)
    credentials =
      ActionController::HttpAuthentication::Basic.encode_credentials(mail, pw)
    request.env["HTTP_AUTHORIZATION"] = credentials
  end
end
