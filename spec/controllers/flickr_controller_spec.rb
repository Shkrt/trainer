require "spec_helper"
require "json_matchers/rspec"
include LoginHelper
include FlickrStubHelper

RSpec.describe Dashboard::FlickrController, type: :controller do
  context "non-authenticated access" do
    describe "POST flickr#search" do
      it "redirects to login page" do
        post :search, keyword: "bar"
        expect(response).to redirect_to(login_path)
      end
    end
  end

  context "authenticated access" do
    let!(:user) { FactoryGirl.create(:user_with_one_block_and_one_card) }

    before(:each) do
      Rails.cache.clear
      stub_request(:post, "https://api.flickr.com/services/rest/").to_return(body: raw_response)
      allow(controller).to receive(:current_user) { user }
    end

    describe "POST flickr#search" do
      it "has a 200 status code" do
        post :search, keyword: "bar"
        expect(response.status).to eq(200)
      end

      it "responds with application/json by default" do
        post :search, keyword: "bar"
        expect(response.content_type).to eq "application/json"
      end

      it "responds with flickr.json schema" do
        post :search, keyword: "bar"
        expect(response).to match_response_schema("flickr")
      end

      it "gets value from cache when it's present in cache" do
        Rails.cache.write("bar", cached_response)
        post :search, keyword: "bar"
        expect(response.body).to eq(cached_response)
        expect(response.body).not_to eq(parsed_response)
      end

      it "gets value from web when it's absent in cache" do
        post :search, keyword: "bar"
        expect(response.body).not_to eq(cached_response)
        expect(eval(response.body)[:flickr]).to eq(parsed_response)
      end

      it "writes value to cache when it's absent in cache" do
        post :search, keyword: "bar"
        expect(Rails.cache.read("bar")).to eq(parsed_response)
      end
    end
  end
end
