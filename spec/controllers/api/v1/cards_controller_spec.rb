require "spec_helper"
require "json_matchers/rspec"
include AuthHelper

def card_obj(usr)
  block = usr.cards.first.block_id
  { "original_text" => "12", "translated_text" => "13",
    "block_id" => block, "review_date" => (-1).days.from_now }
end

RSpec.describe Api::V1::CardsController, type: :controller do
  let!(:user) { FactoryGirl.create(:user_with_one_block_and_one_card) }
  before { allow(controller).to receive(:current_user) { user } }

  context "non-authenticated access" do
    describe "GET cards#index" do
      it "has a 401 status code" do
        get :index
        expect(response.status).to eq(401)
      end
    end

    describe "POST cards#index" do
      context "when invalid parameters passed" do
        it "has a 401 status code" do
          card = card_obj(user)
          card[:review_date] = nil
          post :create, "card" => card
          expect(response.status).to eq(401)
        end
      end

      context "when correct parameters passed" do
        it "has a 401 status codet" do
          post :create, "card" => card_obj(user)
          expect(response.status).to eq(401)
        end

        it "has a 401 status code" do
          post :create, "card" => card_obj(user)
          expect(response.status).to eq(401)
        end
      end
    end
  end

  context "authenticated access" do
    before(:each) do
      basic_auth(user.email, user.password)
    end

    describe "GET cards#index" do
      it "has a 200 status code" do
        get :index
        expect(response.status).to eq(200)
      end

      it "response represents a valid cards object" do
        get :index
        expect(response).to match_response_schema("cards")
      end

      it "responds to ajax post by default" do
        get :index
        expect(response.content_type).to eq "application/json"
      end
    end

    describe "POST cards#index" do
      context "when invalid parameters passed" do
        it "has a 400 status code" do
          card = card_obj(user)
          card["translated_text"] = card["original_text"]
          post :create, "card" => card
          expect(response.status).to eq(400)
        end
      end

      context "when correct parameters passed" do
        it "responds with a newly created card object" do
          post :create, "card" => card_obj(user)
          expect(response).to match_response_schema("card")
        end

        it "has a 200 status code" do
          post :create, "card" => card_obj(user)
          expect(response.status).to eq(200)
        end

        it "responds to ajax post by default" do
          post :create, "card" => card_obj(user)
          expect(response.content_type).to eq "application/json"
        end
      end
    end
  end
end
