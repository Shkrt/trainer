require "spec_helper"
require "json_matchers/rspec"
include AuthHelper

def review_result(resp)
  JSON.parse(resp.body)["state"]["state"]
end

describe Api::V1::TrainerController, type: :controller do
  let!(:user) { FactoryGirl.create(:user_with_one_block_and_one_card) }
  before { allow(controller).to receive(:current_user) { user } }

  context "non-authenticated access" do
    describe "GET index" do
      it "has a 401 status code" do
        get :index
        expect(response.status).to eq(401)
      end
    end

    describe "PUT review_card" do
      it "renders a 401 status code when answer is incorrect" do
        card_id = user.cards.first.id
        put :review_card, user_translation: "trrr", card_id: card_id
        expect(response.status).to eq(401)
      end

      it "renders a 401 status code when answer is correct" do
        card_id = user.cards.first.id
        put :review_card, user_translation: "house", card_id: card_id
        expect(response.status).to eq(401)
      end
    end
  end

  context "authenticated access" do
    before(:each) do
      basic_auth(user.email, user.password)
    end

    describe "GET index" do
      it "has a 200 status code" do
        get :index
        expect(response.status).to eq(200)
      end

      it "response represents a valid card object" do
        get :index
        expect(response).to match_response_schema("card")
      end

      it "responds to ajax post by default" do
        get :index
        expect(response.content_type).to eq "application/json"
      end
    end

    describe "PUT review_card" do
      it "renders a valid result object when answer is incorrect" do
        card_id = user.cards.first.id
        put :review_card, user_translation: "trrr", card_id: card_id
        expect(response).to match_response_schema("result")
        expect(review_result(response)).to be false
      end

      it "renders a valid result object when answer is correct" do
        card_id = user.cards.first.id
        put :review_card, user_translation: "house", card_id: card_id
        expect(response).to match_response_schema("result")
        expect(review_result(response)).to be true
      end
    end
  end
end
