require "spec_helper"
include LoginHelper

RSpec.describe Dashboard::LoadCardsController, type: :controller do
  context "non-authenticated access" do
    describe "GET load_cards#new" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to(login_path)
      end
    end

    describe "POST load_cards#fetch_cards" do
      it "redirects to login page" do
        post :fetch_cards, something_that_really: :doesnt_matter
        expect(response).to redirect_to(login_path)
      end
    end
  end

  context "authenticated access" do
    let!(:user) { FactoryGirl.create(:user_with_one_block_and_one_card) }
    before { allow(controller).to receive(:current_user) { user } }

    describe "GET load_cards#new" do
      it "responds with 200" do
        get :new
        expect(response.status).to eq(200)
      end
    end

    describe "POST load_cards#fetch_cards" do
      it "processes fetch_cards request" do
        post :fetch_cards, any: :params
        expect(response.status).to redirect_to(root_path)
      end
    end

    context "when user has no blocks" do
      let!(:user) { FactoryGirl.create(:user) }
      describe "GET load_cards#new" do
        it "redirects to new_block_path" do
          get :new
          expect(response.status).to redirect_to(new_block_path)
        end
      end
    end
  end
end
