require "rails_helper"

describe User do
  let!(:user) { create(:user) }
  let!(:block) { create(:block, user: user) }
  let!(:card) { create(:card, user: user, block: block) }

  context "When current block exists" do
    it ".get_card takes card from current block if block contains at least one card" do
      user.set_current_block(block)
      block_1 = create(:block, user: user)
      card_1 = create(:card, user: user, block: block_1)
      expect(user.get_card).to eq card
      expect(user.get_card).not_to eq card_1
    end

    it ".get_card returns nil if block is empty, but other blocks have cards" do
      block_1 = create(:block, user: user)
      user.set_current_block(block_1)
      expect(user.get_card).to be nil
    end
  end

  context "When current block doesn't exist" do
    it ".get_card takes any card if there is at least one card present in the system" do
      expect(user.get_card).to eq card
    end

    it ".get_card returns nil if there are no cards at all" do
      Card.destroy(card.id)
      expect(user.get_card).to be nil
    end
  end
end
