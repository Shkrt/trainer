require "rails_helper"

describe "CardsFetch" do
  let(:link) { "http://www.not_really_exist.com" }

  before(:each) do
    stub_request(:get, link).to_return(body: body, status: 200)
  end

  context "when user input is consistent" do
    let(:result) { CardsFetch.grab(link, "td:nth-child(3)", "td:nth-child(2)") }

    context "when search was successful" do
      let (:body) { "<tr><td>1</td><td>2</td><td>3</td></tr><tr><td>4</td><td>5</td><td>6</td></tr>" }

      it ".grab returns an array of arrays" do
        expect(result).to be_instance_of(Array)
        result.each { |x| expect(x).to be_instance_of(Array) }
      end

      it ".grab results in array which length is equal to half of found selectors count" do
        expect(result.length).to eq 2
      end
    end

    context "when nothing was found" do
      let (:body) { "nothing" }

      it ".grab returns an empty array of arrays" do
        expect(result).to be_instance_of(Array)
        expect(result.length).to eq 0
        result.each { |x| expect(x).to be_instance_of(Array) }
      end
    end
  end

  context "when incorrect css selectors passed" do
    let(:result) { CardsFetch.grab(link, "13", "12") }
    let (:body) { "" }

    it ".grab returns nil" do
      expect(result).to be_nil
    end
  end
end
