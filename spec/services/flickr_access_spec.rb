require "rails_helper"
require "support/matchers/url_matcher"
include FlickrStubHelper

describe "FlickrAccess" do
  let(:response) { raw_response }

  before(:each) do
    stub_request(:post, "https://api.flickr.com/services/rest/").to_return(body: response)
  end

  it ".get_by_keyword returns an array of given length" do
    arr = FlickrAccess.new.get_by_keyword("test", 6)
    expect(arr).to be_instance_of(Array)
    expect(arr.length).to eq 6
  end

  it ".get_by_keyword returns an array of valid url strings" do
    arr = FlickrAccess.new.get_by_keyword("test", 6)
    expect(arr).to all(be_url)
  end
end
