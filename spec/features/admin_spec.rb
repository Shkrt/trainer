require "rails_helper"
require "support/helpers/login_helper.rb"
include LoginHelper

context "Non-authenticated user" do
  it "has not access to the administrator dashboard" do
    visit admin_root_path
    expect(page).to have_content I18n.t(:please_log_in_alert)
  end

  it "does not see the link to administrator dashboard" do
    visit root_path
    expect(page).not_to have_content I18n.t("active_admin.dashboard")
  end
end

context "Authenticated user without administrator rights" do
  before do
    create(:user)
    visit trainer_path
    login("test@test.com", "12345", "#login_btn")
  end

  it "has not access to the administrator dashboard" do
    visit admin_root_path
    expect(page).to have_content I18n.t(:not_authorized_alert)
  end

  it "does not see the link to administrator dashboard" do
    visit root_path
    expect(page).not_to have_content I18n.t("active_admin.dashboard")
  end
end

context "Authenticated user with administrator rights" do
  before do
    create(:user_with_administrator_privileges)
    visit trainer_path
    login("test@test.com", "12345", "#login_btn")
  end

  it "sees the link to administrator dashboard" do
    visit root_path
    expect(page).to have_content I18n.t("active_admin.dashboard")
  end
end
