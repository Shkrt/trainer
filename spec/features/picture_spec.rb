require "rails_helper"
require "support/helpers/login_helper.rb"
include LoginHelper

describe "Attaching images" do
  let!(:user) { create(:user_with_one_block_and_one_card) }
  let!(:card) { user.cards.first }

  before(:each) do
    visit trainer_path
    login("test@test.com", "12345", "#login_btn")
  end

  context "when card has not attached picture" do
    it "user sees both options: attach remote picture and upload from file" do
      visit edit_card_path(card)
      expect(page).to have_content I18n.t("card.image")
      expect(page).to have_content I18n.t("card.search_image")
    end
  end

  context "when card has link to picture at remote storage" do
    it "user sees option for deletion of file and does not see file upload link" do
      card.update_attribute(:remote_image, "mock.jpg")
      visit edit_card_path(card)
      expect(page).to have_content I18n.t("card.remove_image")
      expect(page).not_to have_content I18n.t("card.image")
    end
  end
end

describe "js features", js: true do
  let!(:user) { create(:user_with_one_block_and_one_card) }
  let!(:card) { user.cards.first }

  before(:each) do
    visit trainer_path
    login("test@test.com", "12345", "#login_btn")
  end

  it "user sees text field when 'Search images at flickr' pressed" do
    visit edit_card_path(card)
    click_on I18n.t("card.search_image")
    expect(page).to have_selector ("#search_field")
  end

  it "user sees image container when 'Search images' submits search value" do
    visit edit_card_path(card)
    click_on :flickr_search_btn
    fill_in :search_field, with: "test"
    click_on :flickr_show_btn
    expect(page).to have_selector (".multiple-items")
  end

  it "user won't see image container when 'Search images' submits empty value" do
    visit edit_card_path(card)
    click_on :flickr_search_btn
    click_on :flickr_show_btn
    expect(page).not_to have_selector (".multiple-items.jumbotron")
  end
end
