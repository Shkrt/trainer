require "rails_helper"
require "support/helpers/login_helper.rb"
include LoginHelper

def submit_seed
  fill_in "remote_url", with: "test"
  fill_in "original_selector", with: "test"
  fill_in "translated_selector", with: "test"
  click_button I18n.t("dashboard.load_cards.new.action")
end

describe "When fetch cards from remote source feature used" do
  context "Non-authenticated user" do
    it "won't see add cards from remote link" do
      visit root_path
      expect(page).not_to have_content I18n.t("add_cards_from_remote")
    end
  end

  context "Authenticated user" do
    context "without decks" do
      before do
        create(:user)
        visit trainer_path
        login("test@test.com", "12345", "#login_btn")
      end

      it "sees add cards from remote link" do
        visit root_path
        expect(page).to have_content I18n.t("add_cards_from_remote")
      end

      it "sees add deck proposal when tries to add cards from remote source" do
        visit new_path
        expect(page).to have_content I18n.t("no_blocks_alert")
      end
    end

    context "that has at least one deck" do
      before do
        create(:user_with_one_block_and_one_card)
        visit trainer_path
        login("test@test.com", "12345", "#login_btn")
      end

      it "sees job started notification" do
        visit new_path
        submit_seed
        expect(page).to have_content I18n.t("job_enqueued")
      end
    end
  end
end
