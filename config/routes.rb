require "sidekiq/web"

Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  filter :locale

  root 'main#index'

  scope module: 'home' do
    resources :user_sessions, only: [:new, :create]
    resources :users, only: [:new, :create]
    get 'login' => 'user_sessions#new', :as => :login

    post 'oauth/callback' => 'oauths#callback'
    get 'oauth/callback' => 'oauths#callback'
    get 'oauth/:provider' => 'oauths#oauth', as: :auth_at_provider
  end

  scope module: 'dashboard' do
    resources :user_sessions, only: :destroy
    resources :users, only: :destroy
    post 'logout' => 'user_sessions#destroy', :as => :logout

    resources :cards

    resources :blocks do
      member do
        put 'set_as_current'
        put 'reset_as_current'
      end
    end

    put 'review_card' => 'trainer#review_card'
    get 'trainer' => 'trainer#index'

    get 'profile/:id/edit' => 'profile#edit', as: :edit_profile
    put 'profile/:id' => 'profile#update', as: :profile

    post "image_search" => "flickr#search"
    post "remove_picture" => "cards#remove_picture"

    get "new" => "load_cards#new"
    post "fetch_cards" => "load_cards#fetch_cards"
  end

  namespace :api do
    namespace :v1 do
      get "/" => "docs#index"
      get "cards" => "cards#index"
      post "cards" => "cards#create"
      put "review_card" => "trainer#review_card"
      get "trainer" => "trainer#index"
    end
  end

  mount SwaggerEngine::Engine, at: "/api-docs"
  mount Sidekiq::Web => "/sidekiq"
end
