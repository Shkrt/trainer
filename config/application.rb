require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Flashcards
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller
    config.active_record.raise_in_transactional_callbacks = true
    config.i18n.default_locale = :ru
    config.i18n.available_locales = [:ru, :en]
    config.active_job.queue_adapter = :sidekiq
    config.cache_store = :redis_store, "redis://localhost:6379/0/cache"
  end
end
