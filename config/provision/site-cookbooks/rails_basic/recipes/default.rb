include_recipe 'build-essential::default'
include_recipe 'zlib'
include_recipe 'ruby_deps'
include_recipe 'ruby_build'
include_recipe 'postgresql::server'
include_recipe 'nodejs'

ruby_build_ruby "2.2.2" do
  prefix_path "/usr/"
  action:install
end

gem_package 'bundler' do
  gem_binary '/usr/bin/gem'
  options '--no-ri --no-rdoc'
end

gem_package 'rails' do
  gem_binary '/usr/bin/gem'
  options '--no-ri --no-rdoc'
end

execute 'bundle install' do
  user 'vagrant'
  cwd '/vagrant'
  command 'bundle install --deployment'
end

template '/vagrant/config/database.yml' do
  source 'database.yml'
  owner 'vagrant'
  mode '644'
end

execute 'create database' do
  user 'vagrant'
  cwd '/vagrant'
  command 'bundle exec rake db:create'
end

execute 'run migrations' do
  user 'vagrant'
  cwd '/vagrant'
  command 'bundle exec rake db:migrate'
end

execute 'start app server' do
  user 'vagrant'
  cwd '/vagrant'
  command 'rails s -b 0.0.0.0'
end
