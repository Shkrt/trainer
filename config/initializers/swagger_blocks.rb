require "swagger/blocks"

module ApiFlashcards
  include Swagger::Blocks

  host = if ENV["RAILS_ENV"] == "production"
           "fathomless-tor-7543.herokuapp.com/api"
         else
           "0.0.0.0:3000/api"
         end

  swagger_root do
    key :swagger, "2.0"
    info do
      key :version, "1.0.0"
      key :title, "Flashcards API"
      key :description, "Flashcards API"
    end
    tag do
      key :name, "Cards"
      key :description, "Card operations"
    end

    tag do
      key :name, "Trainer"
      key :description, "Trainer operations"
    end

    key :host, host
    key :basePath, "/v1"
    key :produces, ["application/json"]
  end

  SWAGGERED_CLASSES = [Api::V1::CardsController, Api::V1::TrainerController,
                       Api::V1::CardModel, Api::V1::ErrorModel,
                       Api::V1::TrainerModel, self].freeze

  swagger_data = Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
  File.open(Rails.root.to_s + "/lib/swagger_engine/swagger.json", "w") do |file|
    file.write(swagger_data.to_json)
  end
end
